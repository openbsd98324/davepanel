/*
 MIT License, Free Software
 A very small code.
 
 Dave Panel
  Inspired by a little code....

 cc: 
 cc davepanel.c -o davepanel
 ./davepanel 

  The code needs to be cleaned-up. anyhow, it works on FreeBSD, NetBSD, and OpenBSD, including GNU/Linux.
  snprintf() should replace strncat()
*/



// Running on BSD  
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <termios.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <ctype.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>  
#include <time.h>
#if defined(__linux__) //linux
#elif defined(_WIN32)
#elif defined(_WIN64)
#elif defined(__unix__) 
#define PATH_MAX 2500
#else
#endif

//int  color_mode = 2;  
int color_mode[3];
int var_dsp_bottomstatus = 1; 
static const time_t default_time = 1230728833;
static const char default_format[] = "%a %b %d %Y";
// ctrl+f = 6 
#define ESC "\033"
#define home() 			printf(ESC "[H") //Move cursor to the indicated row, column (origin at 1,1)
#define clrscr()		printf(ESC "[2J") //clear clr move to (1,1)
#define gotoxy(x,y)		printf(ESC "[%d;%dH", y, x);
#define ansigotoyx(y,x)		printf(ESC "[%d;%dH", y, x);

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"




int  rows, cols ; 
int  pansel = 1;
int  show_path_title = 1; 
char file_filter[3][PATH_MAX];
char fileclip_selection[7][PATH_MAX];
char pathpan[5][PATH_MAX];
char pathclipboard[3][PATH_MAX];
char dirslot[11][PATH_MAX];
char clipboard_filter[PATH_MAX];
int  scrollyclipboard[3] ;
int  selclipboard[3] ;
char clipboard[PATH_MAX];
int  var_run_x  = 0;
int  var_panel_rcn_rcname = 0;
int  nexp_user_sel[5] ; 
int  nexp_user_scrolly[5] ;
char targetfileselect[PATH_MAX]; 
int  tc_det_dir_type = 1;



char *fbasename(char *name)
{
  char *base = name;
  while (*name)
    {
      if (*name++ == '/')
	{
	  base = name;
	}
    }
  return (base);
}





///////////////////////////////////////////
void readfile( char *filesource )
{
   FILE *source; 
   int ch ; 
   source = fopen( filesource , "r");
   if ( source == NULL ) { printf( "File not found.\n" ); } else {
   while( ( ch = fgetc(source) ) != EOF )
   {
         printf( "%c", ch );
   }
   fclose(source);
   }
}
   









void nsystem( char *mycmd )
{
   printf( "<SYSTEM>\n" );
   printf( " >> CMD:%s\n", mycmd );
   system( mycmd );
   printf( "</SYSTEM>\n");
}





void xnrunwith( char *cmdapp, char *filesource )
{
           char cmdi[PATH_MAX];
           strncpy( cmdi , "  " , PATH_MAX );
           if      ( var_run_x == 1 ) 
              strncpy( cmdi , "  export DISPLAY=:0 ; " , PATH_MAX );
           else if ( var_run_x == 2 ) 
              strncpy( cmdi , "  export DISPLAY=:0 ; screen -d -m  " , PATH_MAX );
           else if ( var_run_x == 0 ) 
              strncpy( cmdi , "  " , PATH_MAX );
           else 
              strncpy( cmdi , "  " , PATH_MAX );
           strncat( cmdi , cmdapp , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi ,  filesource , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
           nsystem( cmdi ); 
}





void nrunwith( char *cmdapp, char *filesource )
{
           char cmdi[PATH_MAX];
           strncpy( cmdi , "  " , PATH_MAX );
           strncat( cmdi , cmdapp , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi ,  filesource , PATH_MAX - strlen( cmdi ) -1 );
           strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
           nsystem( cmdi ); 
}



void nfltk( char *cmdapp, char *filearg )
{
          char cmdi[PATH_MAX];
          strncpy( cmdi , " c++  -I/usr/X11R7/include/ -I/usr/pkg/include -I /usr/pkg/include/    -L/usr/pkg/lib  -L/usr/X11R7/lib -lX11 -I /usr/X11R7/include/   -lfltk   " , PATH_MAX - strlen( cmdi ) -1 );
          strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
          strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
          strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
          strncat( cmdi ,  filearg , PATH_MAX - strlen( cmdi ) -1 );
          strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
          strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
          strncat( cmdi , " -o /tmp/a.out ; env  LD_LIBRARY_PATH='/usr/X11R7/lib:/usr/pkg/lib'  TZ=Europe/Amsterdam  /tmp/a.out  " , PATH_MAX - strlen( cmdi ) -1 );
          printf(  "|Command: %s|\n" , cmdi );
          nsystem( cmdi );
}


void clear_scr()
{
    int fooi;
    struct winsize w; // need ioctl and unistd 
    ioctl( STDOUT_FILENO, TIOCGWINSZ, &w );
    clrscr();
    home();
}





int filecheckexist(char *a_option)
{
  char dir1[PATH_MAX]; 
  char *dir2;
  DIR *dip;
  strncpy( dir1 , "",  PATH_MAX  );
  strncpy( dir1 , a_option,  PATH_MAX  );

  struct stat st_buf; 
  int status; 
  int fileordir = 0 ; 

  status = stat ( dir1 , &st_buf);
  if (status != 0) {
    fileordir = 0;
  }

  // this is compatible to check if a file exists
  FILE *fp2check = fopen( dir1  ,"r");
  if( fp2check ) {
  // exists
  fileordir = 1; 
  fclose(fp2check);
  } 

  if (S_ISDIR (st_buf.st_mode)) {
    fileordir = 2; 
  }
return fileordir;
/////////////////////////////
}







static struct termios oldt;

void restore_terminal_settings(void)
{
    tcsetattr(0, TCSANOW, &oldt);  /* Apply saved settings */
}

void enable_waiting_for_enter(void)
{
    tcsetattr(0, TCSANOW, &oldt);  /* Apply saved settings */
}

void disable_waiting_for_enter(void)
{
    struct termios newt;

    /* Make terminal read 1 char at a time */
    tcgetattr(0, &oldt);  /* Save terminal settings */
    newt = oldt;  /* Init new settings */
    newt.c_lflag &= ~(ICANON | ECHO);  /* Change settings */
    tcsetattr(0, TCSANOW, &newt);  /* Apply settings */
    atexit(restore_terminal_settings); /* Make sure settings will be restored when program ends  */
}








/////////////////////////
/////////////////////////
void nls()
{ 
   DIR *dirp;
   struct dirent *dp;
   dirp = opendir( "." );
   while  ((dp = readdir( dirp )) != NULL ) 
   {
         if (  strcmp( dp->d_name, "." ) != 0 )
         if (  strcmp( dp->d_name, ".." ) != 0 )
             printf( "%s\n", dp->d_name );
   }
   closedir( dirp );
}




void printdir( int pyy, int fopxx, char *mydir , int panviewpr )
{
   //int pxx = fopxx ;
   //if ( pxx == 0 ) pxx = 2;
   int pxx = fopxx;
   if ( pxx == 0 ) pxx = 2;
   DIR *dirp; int posy = 0;  int posx, chr ; 
   int fooselection = 0;

   posy = 1; posx = cols/2;

   char cwd[PATH_MAX];
   struct dirent *dp;
   dirp = opendir( mydir  );
   int entrycounter = 0;
   fooselection = 0;
   while  ((dp = readdir( dirp )) != NULL ) 
   if ( posy <= rows-3 )
   {
        entrycounter++;
        if ( entrycounter <= nexp_user_scrolly[panviewpr] )
              continue;

        if ( strcmp(  file_filter[panviewpr] , "" ) != 0 ) 
        {
           if ( strstr( dp->d_name, file_filter[panviewpr] ) == 0 ) 
              continue;
        }

        printf("%s", KNRM);
        if      ( color_mode[panviewpr] == 2 ) printf("%s", KCYN );
        else if ( color_mode[panviewpr] == 3 ) printf("%s", KGRN );

        if (  dp->d_name[0] !=  '.' ) 
        if (  strcmp( dp->d_name, "." ) != 0 )
        if (  strcmp( dp->d_name, ".." ) != 0 )
        {
            posy++;  fooselection++;

            // b2s
            if ( color_mode[panviewpr] != 4 )    // turbo is mode 4
	    {
             if ( dp->d_type == DT_DIR ) 
             {
                 ansigotoyx( posy, pxx );
                 printf( "/" );
                 posx++;
                 printf("%s", KYEL);
                 if           ( color_mode[panviewpr] == 1 ) printf("%s", KGRN );
                 else if      ( color_mode[panviewpr] == 2 ) printf("%s", KYEL );
                 else if      ( color_mode[panviewpr] == 3 ) printf("%s", KMAG );
             }
             else if ( dp->d_type == 0 )
             {
               if ( tc_det_dir_type == 1 )
               if ( filecheckexist( dp->d_name ) == 2 )
               {
                 ansigotoyx( posy, pxx );
                 printf( "/" );
                 posx++;
                 printf("%s", KCYN);
                 if           ( color_mode[panviewpr] == 1 ) printf("%s", KGRN );
                 else if      ( color_mode[panviewpr] == 2 ) printf("%s", KYEL );
                 else if      ( color_mode[panviewpr] == 3 ) printf("%s", KMAG );
               }
              }
	     } // b2e

            if ( nexp_user_sel[ panviewpr ] == fooselection ) 
            {
                  if ( panviewpr == pansel )
                  {
                    ansigotoyx( posy, pxx-1 );
                    //ansigotoyx( posy, fopxx );
                    strncpy( targetfileselect, dp->d_name , PATH_MAX );
                    printf( ">" );
                  }
            }
            else 
            {
                  ansigotoyx( posy, fopxx );
                  printf( " " );
            }


            ansigotoyx( posy, pxx );
            for ( chr = 0 ;  chr <= strlen(dp->d_name) ; chr++) 
            {
              if  ( dp->d_name[chr] == '\n' )
              {    //posx = cols/2;
              }
              else if  ( dp->d_name[chr] == '\0' )
              {    //posx = cols/2;
              }
              else
              {  
                 //mvaddch( posy, posx++ , dp->d_name[chr] );
                 printf( "%c", dp->d_name[chr] );
                 posx++;
              }
            }
        }
   }
   closedir( dirp );
   printf("%s", KNRM);
}

















char userstr[PATH_MAX];
/////////////////////////////////////
void strninput( char *mytitle, char *foostr )
{
      strncpy( userstr , "" , PATH_MAX );
      disable_waiting_for_enter();
      char strmsg[PATH_MAX];
      char charo[PATH_MAX];
      int foousergam = 0; int ch ;  int chr;

      strncpy( strmsg, ""  ,  PATH_MAX );
      strncpy( strmsg, foostr , PATH_MAX );

      int j; 
      char ptr[PATH_MAX];
      char str[PATH_MAX];

      while( foousergam == 0 ) 
      {

         ansigotoyx( rows, 0 );
         for ( chr = 0 ;  chr <= cols-1 ; chr++) printf( " ");
         ansigotoyx( rows, 0 );
         printf( ": %s", strmsg );

         ch = getchar();
         if ( ch == 10 )            foousergam = 1;

	 else if ( ch == 27 ) 
	      strncpy( strmsg, ""  ,  PATH_MAX );

	 else if ( ch == 2 ) 
	      strncpy( strmsg, ""  ,  PATH_MAX );

	 else if ( ch == 4 ) 
	 {      
            snprintf( charo, PATH_MAX , "%s%d",  strmsg, (int)time(NULL));
	    strncpy( strmsg,  charo ,  PATH_MAX );
         }

	 else if ( ( ch == 8 )  || ( ch == 127 ) )  
         {
            if ( strlen( strmsg ) >= 2 ) 
            {
              j = 0; strncpy(  ptr , "" ,  PATH_MAX );
              for ( chr = 0 ;  chr <= strlen( strmsg )-2 ; chr++) 
              {
                 ptr[j++] = strmsg[chr];
              }
	      strncpy( strmsg, ptr  ,  PATH_MAX );
            }
            else
	      strncpy( strmsg, ""  ,  PATH_MAX );
         }

	 else if (
			(( ch >= 'a' ) && ( ch <= 'z' ) ) 
		        || (( ch >= 'A' ) && ( ch <= 'Z' ) ) 
		        || (( ch >= '1' ) && ( ch <= '9' ) ) 
		        || (( ch == '0' ) ) 
		        || (( ch == '~' ) ) 
		        || (( ch == '!' ) ) 
		        || (( ch == '&' ) ) 
		        || (( ch == '=' ) ) 
		        || (( ch == ':' ) ) 
		        || (( ch == ';' ) ) 
		        || (( ch == '<' ) ) 
		        || (( ch == '>' ) ) 
		        || (( ch == ' ' ) ) 
		        || (( ch == '|' ) ) 
		        || (( ch == '#' ) ) 
		        || (( ch == '?' ) ) 
		        || (( ch == '+' ) ) 
		        || (( ch == '/' ) ) 
		        || (( ch == '\\' ) ) 
		        || (( ch == '.' ) ) 
		        || (( ch == '$' ) ) 
		        || (( ch == '%' ) ) 
		        || (( ch == '-' ) ) 
		        || (( ch == ',' ) ) 
		        || (( ch == '{' ) ) 
		        || (( ch == '}' ) ) 
		        || (( ch == '(' ) ) 
		        || (( ch == ')' ) ) 
		        || (( ch == ']' ) ) 
		        || (( ch == '[' ) ) 
		        || (( ch == '*' ) ) 
		        || (( ch == '"' ) ) 
		        || (( ch == '@' ) ) 
		        || (( ch == '-' ) ) 
		        || (( ch == '_' ) ) 
		        || (( ch == '^' ) ) 
		        || (( ch == '\'' ) ) 
	             ) 
		  {
                        snprintf( charo, PATH_MAX , "%s%c",  strmsg, ch );
		        strncpy( strmsg,  charo ,  PATH_MAX );
		  }
     }
     strncpy( userstr, strmsg , PATH_MAX );
}



///////////////////////////////////////////
void printhline( )
{
   int ch; 
   for ( ch = 0 ;  ch <= cols-1 ; ch++) printf( "%c", '-');
}







///////////////////////////////////////////
void readfilesp( char *filesource, int linestart , int lineend )
{
  FILE *source; 
  int ch ;  int linecount = 1;
  if ( filecheckexist( filesource ) == 1 )
  {
    source = fopen( filesource , "r");
    //if ( source == NULL ) { printf( "File not found.\n" ); } else  
    {
     clear_scr();
     for ( ch = 0 ;  ch <= cols-1 ; ch++) printf( "%c", '-');
     printf( "FILE: %s\n", filesource );
     for ( ch = 0 ;  ch <= cols-1 ; ch++) printf( "%c", '-');
     printf( "%d: ", linecount );
     while( ( ch = fgetc(source) ) != EOF )
     {
         if ( linecount <= lineend ) 
         {
           if ( ch == '\n' ) 
           {
              linecount++; 
              printf( "\n%d: ", linecount );
           }
           else
              printf( "%c", ch );
         }
     }
     fclose(source);
     }
  
     printf( "\33[2K" ); 
     printf( "\r" );
     for ( ch = 0 ;  ch <= cols-1 ; ch++) printf( "%c", '-');
   }
}
   






void printat( int y1, int x1, char *mystring )
{
         ansigotoyx( y1 , x1 );  
         printf( "%s", mystring );
}
void mvcenter( int myposypass, char *mytext )
{
      printat( myposypass , cols/2 - strlen( mytext )/2  , mytext );
}
void gfxhline( int y1 , int x1 , int x2 , int mychar )
{
    int foo, fooy , foox ;
    foo = x1;
    ansigotoyx( y1 , x1 );  
    for( foox = x1 ; foox <= x2 ; foox++) 
         printf( "%c", mychar );
}

void gfxrect( int y1, int x1, int y2, int x2 )
{
    int foo, fooy , foox ;
    for( foox = x1 ; foox <= x2 ; foox++) 
    for( fooy = y1 ; fooy <= y2 ; fooy++) 
    {
        ansigotoyx( fooy , foox );  
        printf( " " );
    }
}





void gfxframe( int y1, int x1, int y2, int x2 )
{
    int foo, fooy , foox ;


    foox = x1;
    for( fooy = y1 ; fooy <= y2 ; fooy++) 
    {
        ansigotoyx( fooy , foox );  
        printf(  "|" );
    }
   
    for( fooy = y1 ; fooy <= y2 ; fooy++) 
    {
        ansigotoyx( fooy , x1 );  
        //printf(ESC "[%d;%dH", fooy, x1 );
        printf( "|" );
    }

    foo = x2;
    for( fooy = y1 ; fooy <= y2 ; fooy++) 
    {
         ansigotoyx( fooy , foo );  
         printf( "|" );
    }
    foo = y1;
    for( foox = x1 ; foox <= x2 ; foox++) 
    {
         ansigotoyx( foo , foox );  
         printf( "-" );
    }
    foo = y2;
    for( foox = x1 ; foox <= x2 ; foox++) 
    {
         ansigotoyx( foo , foox );  
         printf( "-" );
    }
}















///////////////// new
char *fextension(char *str)
{ 
    char ptr[strlen(str)+1];
    int i,j=0;
    //char ptrout[strlen(ptr)+1];  
    char ptrout[25];

    if ( strstr( str, "." ) != 0 )
    {
      for(i=strlen(str)-1 ; str[i] !='.' ; i--)
      {
        if ( str[i] != '.' ) 
            ptr[j++]=str[i];
      } 
      ptr[j]='\0';

      j = 0; 
      for( i=strlen(ptr)-1 ;  i >= 0 ; i--)
            ptrout[j++]=ptr[i];
      ptrout[j]='\0';
    }
    else
     ptrout[0]='\0';

    size_t siz = sizeof ptrout ; 
    char *r = malloc( sizeof ptrout );
    return r ? memcpy(r, ptrout, siz ) : NULL;
}













void proc_load_env( char *foosavefile )
{
         FILE *fptt;
	 int ch, i; 
	 char cwd[PATH_MAX];
	 char getlinestr[PATH_MAX];
	 char string[PATH_MAX];

          printf( "\n");
          //printf( "Press Key... \n" );
          //ch = getchar(); 
          printf( "\nKEY Int:%d char:%c\n", ch , ch );
          printf( "\nKEY Int-48:%d char:%c\n", ch -48 , ch );
          strncpy( string , getcwd( cwd, PATH_MAX ), PATH_MAX );
          chdir( getenv( "HOME" ) );
          if ( filecheckexist( foosavefile ) == 1 )
          {
               fptt = fopen( foosavefile  , "rb+" );
               //for( i = 1 ; i <= ch-48 ; i++)
               fgets( getlinestr , PATH_MAX, fptt ); 
               for( i = 0; i < strlen( getlinestr ); i++ )
               {
                    if( getlinestr[i] == '\n')  getlinestr[i] = '\0';
               }
	       strncpy( pathpan[ 1 ], getlinestr , PATH_MAX );
               fgets( getlinestr , PATH_MAX, fptt ); 
               for( i = 0; i < strlen( getlinestr ); i++ )
               {
                    if( getlinestr[i] == '\n')  getlinestr[i] = '\0';
               }
	       strncpy( pathpan[ 2 ], getlinestr , PATH_MAX );
               fclose( fptt );
               //printf( "\n%s\n", getlinestr );
               //chdir( getlinestr ); 
               nexp_user_sel[ pansel ]=1; nexp_user_scrolly[ pansel ] = 0; 
               //strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
               chdir( pathpan[ pansel ] );
          }
          //chdir( string );
}








void proc_save_env( char *foosavefile )
{
        int ch ;  int i;
        FILE *fptt; 
        char cmdifoo[PATH_MAX];
        char cwd[PATH_MAX];
        printf( "\n");
        printf( "(Save Env Yes / No [y/n]?) (Press Key...)\n" );
	ch = '1';

        if ( ( ch == '1' ) || ( ch == 'y' ) )
        {   
          strncpy( cmdifoo , getcwd( cwd, PATH_MAX ), PATH_MAX );

          chdir( getenv( "HOME" ) );
          fptt = fopen( foosavefile , "wb+" );

          fputs( pathpan[ 1 ] , fptt );
          fputs( "\n" , fptt );

          fputs( pathpan[ 2 ] , fptt );
          fputs( "\n" , fptt );

          //for( i = 1 ; i <= 10 ; i++)
          //     fputs( "/home/\n" , fptt );
          fclose( fptt );

          chdir( cmdifoo ); 
        }
        ch = 0;
}








void void_print_colors()
{
       printf("%s", KCYN);
       printf("%s Colr\n", KNRM  );
       printf("%s Colr\n", KRED  );
       printf("%s Colr\n", KGRN  );
       printf("%s Colr\n", KYEL  );
       printf("%s Colr\n", KBLU  );
       printf("%s Colr\n", KMAG  );
       printf("%s Colr\n", KCYN  );
       printf("%s Colr\n", KWHT  );
       printf("> Color Mode 2:\n");
       printf("%s Colr\n", KMAG  );
       printf("%s Colr\n", KCYN  );
}




////////////////////////////////////////
int main( int argc, char *argv[])
{


    char foostrpwd[PATH_MAX];
    char foostr[PATH_MAX];
    char foocwd[PATH_MAX];
    char getlinestr[PATH_MAX];
    FILE *fptt; 
    char cmdifoo[PATH_MAX];
    int i ; 

    color_mode[ 1 ] = 3; 
    color_mode[ 2 ] = 3;  

    ////////////////////////////////////////////////////////
    if ( argc == 2)
    if ( strcmp( argv[1] , "-colors" ) ==  0 ) 
    {
       void_print_colors();
       return 0;
    }


    ////////////////////////////////////////////////////////
    if ( argc == 2)
    if ( strcmp( argv[1] , "-yellow" ) ==  0 ) 
    {
       printf("%syellow\n", KYEL);
       return 0;
    }

    ////////////////////////////////////////////////////////
    if ( argc == 2)
    if ( strcmp( argv[1] , "-red" ) ==  0 ) 
    {
       printf("%sred\n", KRED);
       return 0;
    }

    ////////////////////////////////////////////////////////
    if ( argc == 2)
    if ( strcmp( argv[1] , "-green" ) ==  0 ) 
    {
       printf("%sgreen\n", KGRN);
       return 0;
    }

      ////////////////////////////////////////////////////////
      if ( argc == 2)
      if ( ( strcmp( argv[1] , "-path" ) ==  0 ) 
      || ( strcmp( argv[1] , "--pwd" ) ==  0 ) 
      || ( strcmp( argv[1] , "-pwd" ) ==  0 ) 
      || ( strcmp( argv[1] , "--path" ) ==  0 ) )
      {
        printf("Path: %s\n", getcwd( foostrpwd, PATH_MAX ) );
        return 0;
      }





     ////////////////////////////////////////////////////////
     if ( argc == 2)
     if ( strcmp( argv[1] , "-t" ) ==  0 ) 
     {
       printf("%d\n", (int)time(NULL));
       return 0;
     }


    struct winsize w; // need ioctl and unistd 
    ioctl( STDOUT_FILENO, TIOCGWINSZ, &w );
    char string[PATH_MAX];
    ////////////////////////////////////////////////////////
    if ( argc == 2)
    if ( ( strcmp( argv[1] , "-size" ) ==  0 ) || ( strcmp( argv[1] , "-screen" ) ==  0 ) )
    {
              printf("Screen\n" );
              printf("Env HOME:  %s\n", getenv( "HOME" ));
              printf("Env PATH:  %s\n", getcwd( string, PATH_MAX ) );
              printf("Env TERM ROW:  %d\n", w.ws_row );
              printf("Env TERM COL:  %d\n", w.ws_col );
              return 0;
    }    


    if ( argc == 2)
    if ( strcmp( argv[1] , "--force" ) ==  0 ) 
    {
              printf("RECOMMENDED TERM ROW: 24 \n" );
              printf("RECOMMENDED TERM COL: 80 \n" );
              printf("Screen\n" );
              printf("Env HOME:  %s\n", getenv( "HOME" ));
              printf("Env PATH:  %s\n", getcwd( string, PATH_MAX ) );
              printf("Env TERM ROW:  %d\n", w.ws_row );
              printf("Env TERM COL:  %d\n", w.ws_col );
	      w.ws_row = 24; 
	      w.ws_col = 80; 
              //return 0;
    }    



    ////////////////////////////////////////////
    if ( argc == 2)
    if ( strcmp( argv[1] , "-info" ) ==  0 ) 
    {
              printf("Screen\n" );
              printf("Env HOME:  %s\n", getenv( "HOME" ));
              printf("Env PATH:  %s\n", getcwd( string, PATH_MAX ) );
              printf("Env TERM ROW:  %d\n", w.ws_row );
              printf("Env TERM COL:  %d\n", w.ws_col );
	      return 0;
    }

    ////////////////////////////////////////////
    if ( argc == 2)
    if ( strcmp( argv[1] , "-term" ) ==  0 ) 
    {
	    printf("Screen\n" );
	    printf("Env HOME:  %s\n", getenv( "HOME" ));
	    printf("Env PATH:  %s\n", getcwd( string, PATH_MAX ) );
	    printf("Env TERM ROW:  %d\n", w.ws_row );
	    printf("Env TERM COL:  %d\n", w.ws_col );
	    return 0;
    }


    int viewpan[5];
    viewpan[ 1 ] = 1;
    viewpan[ 2 ] = 1;
    ////////////////////////////////////////////////////////
    char cwd[PATH_MAX];

    for ( i = 1 ; i <= 10 ; i++ )
       strncpy(  dirslot[ i ], "", PATH_MAX );

    strncpy(  fileclip_selection[ 1 ], "", PATH_MAX );
    strncpy(  fileclip_selection[ 2 ], "", PATH_MAX );
    strncpy(  fileclip_selection[ 4 ], "", PATH_MAX );
    strncpy(  fileclip_selection[ 5 ], "", PATH_MAX );
    strncpy(  clipboard       , "", PATH_MAX );

     ////////////////////////////////////////////////////////
     if ( argc == 2)
     if ( strcmp( argv[1] , "-1" ) ==  0 ) 
     {
           viewpan[ 1 ] = 1;
           viewpan[ 2 ] = 0;
     }

     ////////////////////////////////////////////////////////
     if ( argc == 3)
     if ( strcmp( argv[1] , "-1" ) ==  0 ) 
     {
          viewpan[ 1 ] = 1;
          viewpan[ 2 ] = 0;
          chdir( argv[ 2 ] );
          strncpy( pathpan[ 1 ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
          strncpy( pathpan[ 2 ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
     }

     ////////////////////////////////////////////////////////
     // filed
     if ( argc == 3)
     if ( strcmp( argv[1] , "-f" ) ==  0 ) 
     {
       printf("%syellow\n", KYEL);
       readfile( argv[ 2 ] );
       return 0;
     }


    ////////////////////////////////////////////////////////
    if ( argc == 2)
    if ( strcmp( argv[1] , "" ) !=  0 )
    if ( filecheckexist( argv[1] ) ==  2 )
    {
          chdir( argv[ 1 ] );
          strncpy( pathpan[ 1 ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
          strncpy( pathpan[ 2 ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
    }



    ////////////////////////////////////////////////////////
    nexp_user_sel[1] = 1;
    nexp_user_sel[2] = 1;
    nexp_user_scrolly[1] = 0;
    nexp_user_scrolly[2] = 0;
    strncpy( pathpan[ 1 ] ,  getcwd( cwd, PATH_MAX ), PATH_MAX );
    strncpy( pathpan[ 2 ] ,  getcwd( cwd, PATH_MAX ), PATH_MAX );
    strncpy( pathclipboard[1] , getcwd( cwd, PATH_MAX ), PATH_MAX );
    strncpy( pathclipboard[2] , getcwd( cwd, PATH_MAX ), PATH_MAX );
    strncpy( file_filter[1] ,   "" , PATH_MAX );
    strncpy( file_filter[2] ,   "" , PATH_MAX );
    strncpy( clipboard_filter , "" , PATH_MAX );






    ////////////////////////////////////////////////////////
    if ( argc == 2)
    if ( strcmp( argv[1] , "-s" ) ==  0 ) 
    {
       printf("Screen\n" );
       printf("Env HOME:  %s\n", getenv( "HOME" ));
       printf("Env PATH:  %s\n", getcwd( string, PATH_MAX ) );
       printf("Env TERM ROW:  %d\n", w.ws_row );
       printf("Env TERM COL:  %d\n", w.ws_col );
       return 0;
    }
    rows = w.ws_row ; 
    cols = w.ws_col ; 


    int ch ; 
    int gameover = 0;
    int foo;
    char strpath[PATH_MAX];
    char userstrsel[PATH_MAX];
    char fileexplorer_message[PATH_MAX];
    strncpy( fileexplorer_message, "" , PATH_MAX );

    ////////////////////////////////////////////////////////
    if ( argc == 3)
    {
          viewpan[ 1 ] = 1;
          viewpan[ 2 ] = 1;
          chdir( argv[ 1 ] );
          strncpy( pathpan[ 1 ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
          chdir( argv[ 2 ] );
          strncpy( pathpan[ 2 ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
          chdir( argv[ 1 ] );
    }


    printf( "fileexplorer...\n" );
    printf( "|*1 |[%s]\n", pathpan[ 1 ] );
    printf( "| 2 |[%s]\n", pathpan[ 2 ] );
    printf( "Reading directories...\n" );

    while ( gameover == 0 ) 
    {
       strncpy( targetfileselect, "" , PATH_MAX );
       disable_waiting_for_enter();
       clear_scr();

       ansigotoyx( 0, 0 );
       if ( show_path_title == 1 ) 
       {
         ansigotoyx( 0, 0 );
         if ( pansel == 1 )
           printf( "|*1 |[%s]", pathpan[ 1 ] );
         else 
           printf( "| 1 |[%s]", pathpan[ 1 ] );
  
         ansigotoyx( 0, cols/2 );
         if ( pansel == 2 )
           printf( "|*2 |[%s]", pathpan[ 2 ] );
         else 
           printf( "| 2 |[%s]", pathpan[ 2 ] );
       }

       chdir( pathpan[ 1 ] );
       if ( viewpan[ 1 ] == 1 ) 
          printdir( 0, 0,       "." , 1 );

       chdir( pathpan[ 2 ] );
       if ( viewpan[ 2 ] == 1 ) 
          printdir( 0, cols/2,  "." , 2 );


      if ( var_dsp_bottomstatus == 1 ) 
      {
       ansigotoyx( rows-1, 0 );
       printf( "|%s|F|%d|[%s]", "" , nexp_user_sel[pansel] ,  targetfileselect );
       if ( var_panel_rcn_rcname == 1 ) 
         printf( "|DISPLAY|" );

       ansigotoyx( rows, 0 );
       printf( "| j:Down | k:Up | h:CdParent | l:CdSel | n:ScrollDown | u:ScrollUp | r:Multi |" );
      }
      ansigotoyx( rows, 0 );

      ch = getchar();
      chdir( pathpan[ pansel ] );

      if (ch ==  'Q') 
      {
             clear_scr();
             enable_waiting_for_enter(); 
             gameover = 1;
             printf( "\n" );
             printf( "Bye!\n" );
      }

     /*
      else if (ch ==  'q') 
      {
             clear_scr();
             enable_waiting_for_enter(); 
             gameover = 1;
             printf( "\n" );
             printf( "Bye!\n" );
      }
      */


      else if ( ch == 'y' )
      {
         strncpy( strpath , getcwd( cwd, PATH_MAX ), PATH_MAX );
         chdir( getenv( "HOME" ) );
         strncpy( string , getenv( "HOME" ), PATH_MAX );
         strncat( string , "/" ,          PATH_MAX - strlen( string ) - 1);
         strncat( string , ".clipboard" , PATH_MAX - strlen( string ) - 1);
         fptt = fopen( string , "wb+" );
         fputs( "!fig{" , fptt );
         fputs( targetfileselect , fptt );
         fputs( "}" , fptt );
         fputs( "\n" , fptt );
         fclose( fptt );
         chdir( strpath );
         printf( "Copying to clipboard the selection.\n" );
      }

      else if ( ch == 'Y' )
      {
         strncpy( strpath , getcwd( cwd, PATH_MAX ), PATH_MAX );
         chdir( getenv( "HOME" ) );
         fptt = fopen( ".clipboard", "ab+" );
         fputs( targetfileselect , fptt );
         fputs( "\n" , fptt );
         fclose( fptt );
         chdir( strpath );
         printf( "Copying to clipboard the selection.\n" );
       }



      else if  (ch == 'y') 
      {
           // copy a line to clipboard
           strncpy( string , getcwd( cwd, PATH_MAX ), PATH_MAX );
           chdir( getenv( "HOME" ) );

           strncpy(  clipboard, "", PATH_MAX );
           strncpy(  clipboard, targetfileselect , PATH_MAX );

           fptt = fopen( ".clipboard", "wb+" );
            fputs( targetfileselect , fptt );
            fputs( "\n" , fptt );
           fclose( fptt );

           printf( "Copying to clipboard the selection.\n" );
           chdir( string );
      }


       else if ( ch == 'L' ) 
       { 
          enable_waiting_for_enter();  
          if (  filecheckexist(  targetfileselect    ) == 1 ) 
                 nrunwith(  " less ",  targetfileselect    );   
       }










       //////////////////////
       //////////////////////
       //////////////////////
       else if ( ch == 'O' ) 
       {
           strninput( " Change Directory (chdir).", "" );
           strncpy( string, userstr , PATH_MAX );
           printf("\n" );
           printf("got: \"%s\"\n", string );
           if ( strcmp( string , "" ) != 0 )
           {
               chdir( string ); 
               nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
               strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
           }
       }






       ////////////////////////////
       /// c-o  ctrl o 
       ////////////////////////////
       else if ( ch == 15 )   // working ctrl + o
       {
           strninput( " Change Directory (chdir) ", "" );
           strncpy( string, userstr , PATH_MAX );
           printf("\n" );
           printf("\n" );
           printf("got: \"%s\"\n", string );
           if ( strcmp( string , "" ) != 0 )
           {
               chdir( pathpan[ pansel ] );
               chdir( string ) ; 
               nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
               strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
           }
       }

       ////////////////////////////
       /// c-o  ctrl o 
       ////////////////////////////
       else if ( ch == 15 ) 
       {
           strninput( " Change Directory (chdir) ", "" );
           strncpy( string, userstr , PATH_MAX );
           printf("\n" );
           printf("\n" );
           printf("got: \"%s\"\n", string );
           if ( strcmp( string , "" ) != 0 )
           {
               chdir( pathpan[ pansel ] );
               chdir( string ) ; 
               nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
               strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
           }
       }



      else if ( ch == 'z' )  
      {  enable_waiting_for_enter();  
         nrunwith(  " tless   ",  targetfileselect    ); 
      }




      else if ( ch == 27 )  
      {
        ch = getchar();
        if ( ch == '[' )  
        {
           ch = getchar();
           if ( ch ==      66 )       system( "  export DISPLAY=:0 ;  xdotool  key Down " );
           else if ( ch == 65 )       system( "  export DISPLAY=:0 ;  xdotool  key Up " );
           else if ( ch == 68 )       system( "  export DISPLAY=:0 ;  xdotool  key Left " );
           else if ( ch == 67 )       system( "  export DISPLAY=:0 ;  xdotool  key Right " );
           //if ( ch == 65 )            nexp_user_sel[pansel]--;
           //else if ( ch == 66  )      nexp_user_sel[pansel]++;
        }

        // 27 79 81 : F2
        else if ( ch == 79 )  
        {
           //ch = getchar();
           //if      ( ch ==      81 )       proc_save_append_env();
        }
      }

       

       else if ( ch == '~')      
       {
            chdir( pathpan[ pansel ] );
            chdir( getenv( "HOME" ) );
            nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
            strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
       }

       else if ( ch == '~')      
       {
            chdir( pathpan[ pansel ] );
            chdir( getenv( "HOME" ) );
            nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
            strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
       }

       else if  ( ch == 's') 
       {
            enable_waiting_for_enter();
            clear_scr();
            printf( "========= \n" );
            printf( "\n" );
            printf( "\n" );

            printf("Side sel: %d",  pansel );
            printf( ", Color:%d %d\n", color_mode[1], color_mode[ 2 ] );
            printf( " \n" );
            printf("PATH 1: %s \n", pathpan[ 1 ] );
            printf( " \n" );
            printf("PATH 2: %s \n", pathpan[ 2 ] );
            printf( " \n" );
            printf("File [%d]: \"%s\" \n", filecheckexist(  targetfileselect    ) ,  targetfileselect    );
            printf( " \n" );

            strncpy( string, pathpan[ pansel ] , PATH_MAX );
            strncat( string , "/" , PATH_MAX - strlen( string ) - 1);
            strncat( string , targetfileselect , PATH_MAX - strlen( string ) - 1);
            printf("File sel: \"%s\"\n",  string );
            printf( " \n" );

            if ( strcmp( fileclip_selection[ 1 ], "" ) != 0 )
            {
              printf("File f1: \"%s\"\n",  fileclip_selection[ 1 ] );
              printf( " \n" );
            }
            if ( strcmp( fileclip_selection[ 2 ], "" ) != 0 )
            {
              printf("File f2: \"%s\"\n",  fileclip_selection[ 2 ] );
              printf( " \n" );
            }
            if ( strcmp( fileclip_selection[ 3 ], "" ) != 0 )
            {
              printf("File f3: \"%s\"\n",  fileclip_selection[ 3 ] );
              printf( " \n" );
            }
            if ( strcmp( fileclip_selection[ 4 ], "" ) != 0 )
            {
              printf("File f4: \"%s\"\n",  fileclip_selection[ 4 ] );
              printf( " \n" );
            }

            //////////////////
            ansigotoyx( rows-1, 0 );
            for( foo = 0 ;  foo <= cols-1 ; foo++) printf( " " );
            ansigotoyx( rows-1, 0 ); printf( "<Press Key>" );
            disable_waiting_for_enter();
            getchar();
            //////////////////
        }


       else if ( ch == 'h')      
       {
            chdir( pathpan[ pansel ] );
            chdir( ".." );
            nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
            strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
            strncpy( file_filter[pansel]  , "" , PATH_MAX );
       }
       else if ( ch == 'l')      
       {
            // save 
            chdir( pathpan[ pansel ] );
            strncpy( pathclipboard[pansel] , getcwd( string, PATH_MAX ), PATH_MAX );
            selclipboard[pansel] =      nexp_user_sel[pansel];
            scrollyclipboard[pansel] =  nexp_user_scrolly[pansel];
            strncpy( clipboard_filter , file_filter[pansel] , PATH_MAX );

            // go 
            chdir( pathpan[ pansel ] );
            chdir( targetfileselect );
            nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
            strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
            strncpy( file_filter[pansel]  , "" , PATH_MAX );

            strncpy( userstr, "" , PATH_MAX );
            strncpy( string, userstr , PATH_MAX );
            strncpy( file_filter[pansel]  , userstr , PATH_MAX );
            nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0;
       }






       /// FILE OPERATIONS BEGIN
       else if ( ch == '4') 
       {
                   chdir( pathpan[ pansel ] );
                   //strncpy( string, " cp -a " , PATH_MAX );
                   //strncpy( string, " cp -r -v " , PATH_MAX );
                   strncpy( string, " cp -a " , PATH_MAX );
                   strncat( string , " \"" , PATH_MAX - strlen(string) - 1);
                   strncat( string ,   targetfileselect  , PATH_MAX - strlen(string) - 1);
                   strncat( string , "\" " , PATH_MAX - strlen(string) - 1);
                   strncat( string , "  " , PATH_MAX - strlen(string) - 1);
                   if ( pansel == 2 ) foo = 1; else if ( pansel == 1 ) foo = 2; 
                   strncat( string , " \"" , PATH_MAX - strlen(string) - 1);
                   strncat( string , pathpan[ foo ] ,  PATH_MAX - strlen(string) - 1);
                   strncat( string , "/" ,  PATH_MAX - strlen(string) - 1);
                   strncat( string , fbasename(  targetfileselect ) , PATH_MAX - strlen(string) - 1);
                   strncat( string , "\" " , PATH_MAX - strlen(string) - 1);
                   ansigotoyx( rows, 0 );
                   gfxhline( rows , 0 , cols-1, ' '); 
                   ansigotoyx( rows-1, 0 );
                   gfxhline( rows-1 , 0 , cols-1 , ' ' ); 
                   ansigotoyx( rows, 0 );
                   gfxhline(  rows-1 , 0 , cols-1 , '=' ); 

                   printf( "CMD: %s [y/n]?\n" ,  string );
                   printf( "Answer: Yes or No [y/n]?\n" );
                   printf( "=========================\n" );
                   foo = getchar();
                   if ( ( foo == '1' ) || ( foo == 'y' ) )
                   {   
                      printf( "%d\n", (int)time(NULL));
                      printf( "=====================\n" );
                      nsystem( string );
                      printf( "=====================\n" );
                      printf( "%d\n", (int)time(NULL));
                      printf( "Process Completed (%s).\n", string );
                      printf( "<Press Key>\n" );
                      printf( "=====================\n" );
                      getchar();
                   }
       }


       else if ( ch == '5') 
       {
                   chdir( pathpan[ pansel ] );
                   strncpy( string, " cp -r " , PATH_MAX );
                   strncat( string , " \"" , PATH_MAX - strlen(string) - 1);
                   strncat( string ,   targetfileselect  , PATH_MAX - strlen(string) - 1);
                   strncat( string , "\" " , PATH_MAX - strlen(string) - 1);
                   strncat( string , "  " , PATH_MAX - strlen(string) - 1);
                   if ( pansel == 2 ) foo = 1; else if ( pansel == 1 ) foo = 2; 
                   strncat( string , " \"" , PATH_MAX - strlen(string) - 1);
                   strncat( string , pathpan[ foo ] ,  PATH_MAX - strlen(string) - 1);
                   strncat( string , "/" ,  PATH_MAX - strlen(string) - 1);
                   strncat( string , fbasename(  targetfileselect ) , PATH_MAX - strlen(string) - 1);
                   strncat( string , "\" " , PATH_MAX - strlen(string) - 1);
                   ansigotoyx( rows, 0 );
                   gfxhline( rows , 0 , cols-1, ' '); 
                   ansigotoyx( rows-1, 0 );
                   gfxhline( rows-1 , 0 , cols-1 , ' ' ); 
                   ansigotoyx( rows, 0 );
                   gfxhline(  rows-1 , 0 , cols-1 , '=' ); 
                   printf( "PATH: %s\n" ,  getcwd( foocwd, PATH_MAX ) );
                   printf( "CMD: %s [y/n]?\n" ,  string );
                   printf( "Answer: Yes or No [y/n]?\n" );
                   printf( "=========================\n" );
                   foo = getchar();
                   if ( ( foo == '1' ) || ( foo == 'y' ) )
                      nsystem( string );
        }


        else if ( ch == '6') 
        {
                   chdir( pathpan[ pansel ] );
                   strncpy( string, " mv   " , PATH_MAX );
                   strncat( string , "   \"" , PATH_MAX - strlen(string) - 1);
                   strncat( string ,   targetfileselect  , PATH_MAX - strlen(string) - 1);
                   strncat( string , "\" " , PATH_MAX - strlen(string) - 1);
                   strncat( string , "  " , PATH_MAX - strlen(string) - 1);
                   if ( pansel == 2 ) foo = 1; else if ( pansel == 1 ) foo = 2; 
                   strncat( string , " \"" , PATH_MAX - strlen(string) - 1);
                   strncat( string , pathpan[ foo ] ,  PATH_MAX - strlen(string) - 1);
                   strncat( string , "\" " , PATH_MAX - strlen(string) - 1);
                   ansigotoyx( rows, 0 );
                   gfxhline( rows , 0 , cols-1, ' '); 
                   ansigotoyx( rows-1, 0 );
                   gfxhline( rows-1 , 0 , cols-1 , ' ' ); 
                   ansigotoyx( rows, 0 );
                   gfxhline(  rows-1 , 0 , cols-1 , '=' ); 
                   printf( "CMD: %s [y/n]?\n" ,  string );
                   printf( "Answer: Yes or No [y/n]?\n" );
                   printf( "=========================\n" );
                   foo = getchar();
                   if ( ( foo == '1' ) || ( foo == 'y' ) )
                      nsystem( string );
        }





        else if ( ch == '8') 
        {
                   ansigotoyx( rows-1 , 0 );
                   printhline( );
                   ansigotoyx( rows , 0 );
                   printf("%d\n", (int)time(NULL));
                   snprintf( string , PATH_MAX , "%d-doc.txt", (int)time(NULL));
                   strninput( "", string );
                   strncpy( string, userstr , PATH_MAX );
                   printf("got: \"%s\"\n", string );
                   chdir( pathpan[ pansel ] );
                   strncpy( string, " touch   " , PATH_MAX );
                   strncat( string , " \"" , PATH_MAX - strlen(string) - 1);
                   strncat( string , userstr  , PATH_MAX - strlen(string) - 1);
                   strncat( string , "\" " , PATH_MAX - strlen(string) - 1);
                   strncat( string , "  " , PATH_MAX - strlen(string) - 1);

                   ansigotoyx( rows, 0 );
                   gfxhline( rows , 0 , cols-1, ' '); 
                   ansigotoyx( rows-1, 0 );
                   gfxhline( rows-1 , 0 , cols-1 , ' ' ); 
                   ansigotoyx( rows, 0 );
                   gfxhline(  rows-1 , 0 , cols-1 , '=' ); 

                   printf( "CMD: %s [y/n]?\n" ,  string );
                   printf( "Answer: Yes or No [y/n]?\n" );
                   printf( "=========================\n" );
                   foo = getchar();
                   if ( ( foo == '1' ) || ( foo == 'y' ) )
                      nsystem( string );
        }


        else if ( ( ch == '2') || ( ch == '3') )
        {
                   ansigotoyx( rows-1 , 0 );
                   printhline( );
                   ansigotoyx( rows , 0 );
                   strninput( "",    targetfileselect    );
                   strncpy( string, userstr , PATH_MAX );
                   printf("got: \"%s\"\n", string );

                   chdir( pathpan[ pansel ] );
                   strncpy( string, " mv  " , PATH_MAX );

                   strncat( string , "   \"" , PATH_MAX - strlen(string) - 1);
                   strncat( string ,   targetfileselect  , PATH_MAX - strlen(string) - 1);
                   strncat( string , "\" " , PATH_MAX - strlen(string) - 1);

                   strncat( string , " \"" , PATH_MAX - strlen(string) - 1);
                   strncat( string , userstr  , PATH_MAX - strlen(string) - 1);
                   strncat( string , "\" " , PATH_MAX - strlen(string) - 1);
                   strncat( string , "  " , PATH_MAX - strlen(string) - 1);

                   ansigotoyx( rows, 0 );
                   gfxhline( rows , 0 , cols-1, ' '); 
                   ansigotoyx( rows-1, 0 );
                   gfxhline( rows-1 , 0 , cols-1 , ' ' ); 
                   ansigotoyx( rows, 0 );
                   gfxhline(  rows-1 , 0 , cols-1 , '=' ); 

                   printf( "CMD: %s [y/n]?\n" ,  string );
                   printf( "Answer: Yes or No [y/n]?\n" );
                   printf( "=========================\n" );
                   foo = getchar();
                   if ( ( foo == '1' ) || ( foo == 'y' ) )
                      nsystem( string );
        }


        else if ( ch == '7') 
        {
                   ansigotoyx( rows-1 , 0 );
                   printhline( );
                   ansigotoyx( rows , 0 );
                   strninput( "", "" );
                   strncpy( string, userstr , PATH_MAX );
                   printf("got: \"%s\"\n", string );

                   chdir( pathpan[ pansel ] );
                   strncpy( string, " mkdir   " , PATH_MAX );
                   strncat( string , " \"" , PATH_MAX - strlen(string) - 1);
                   strncat( string , userstr  , PATH_MAX - strlen(string) - 1);
                   strncat( string , "\" " , PATH_MAX - strlen(string) - 1);
                   strncat( string , "  " , PATH_MAX - strlen(string) - 1);

                   ansigotoyx( rows, 0 );
                   gfxhline( rows , 0 , cols-1, ' '); 
                   ansigotoyx( rows-1, 0 );
                   gfxhline( rows-1 , 0 , cols-1 , ' ' ); 
                   ansigotoyx( rows, 0 );
                   gfxhline(  rows-1 , 0 , cols-1 , '=' ); 

                   printf( "CMD: %s [y/n]?\n" ,  string );
                   printf( "Answer: Yes or No [y/n]?\n" );
                   printf( "=========================\n" );
                   foo = getchar();
                   if ( ( foo == '1' ) || ( foo == 'y' ) )
                      nsystem( string );
        }
       /// FILE OPERATIONS END




        else if (  ch == 'b' ) 
        {
            home(); printf( "|BACK|");
            chdir( pathpan[ pansel ] );
            chdir( pathclipboard[pansel] );
            strncpy( pathpan[ pansel ] , getcwd( string, PATH_MAX ), PATH_MAX );
            strncpy( file_filter[pansel], clipboard_filter , PATH_MAX );
            nexp_user_sel[pansel] = 1;   nexp_user_scrolly[pansel] = 0; 
            nexp_user_sel[pansel] =      selclipboard[pansel] ;
            nexp_user_scrolly[pansel] =  scrollyclipboard[pansel] ;
        }










       else if ( ch == 10 ) 
       {
             if      ( strcmp( fextension( targetfileselect ) , "mp4" ) == 0 )
               nrunwith( " mplayer -vo x11 -fs -zoom " , targetfileselect );

             else if ( strcmp( fextension( targetfileselect ) , "cxx" ) == 0 )
               nfltk( "   " , targetfileselect );


             else if ( strcmp( fextension( targetfileselect ) , "mp3" ) == 0 )
               //nrunwith( " splay " , targetfileselect );
               nrunwith( " mpg123 " , targetfileselect );

             else if ( strcmp( fextension( targetfileselect ) , "MP3" ) == 0 )
               //nrunwith( " splay " , targetfileselect );
               nrunwith( " mpg123 " , targetfileselect );



             else if ( strcmp( fextension( targetfileselect ) , "wav" ) == 0 )
	     {
	       if ( filecheckexist( "/etc/wscons.conf" ) == 1 ) 
                  nrunwith( " audioplay " , targetfileselect );
	       else 
                  nrunwith( " aplay " , targetfileselect );
                  //nrunwith( " aucat -i " , targetfileselect );
	     }

             else if ( strcmp( fextension( targetfileselect ) , "ppm" ) == 0 )
                 nrunwith( "   feh  " , targetfileselect );

             else if ( strcmp( fextension( targetfileselect ) , "png" ) == 0 )
                 nrunwith( "   mupdf   " , targetfileselect );

             else if ( strcmp( fextension( targetfileselect ) , "PNG" ) == 0 )
                 nrunwith( "   mupdf   " , targetfileselect );

             else if ( strcmp( fextension( targetfileselect ) , "jpg" ) == 0 )
                 nrunwith( "   mupdf   " , targetfileselect );

             else if ( strcmp( fextension( targetfileselect ) , "docx" ) == 0 )
                 nrunwith( "   libreoffice    " , targetfileselect );

             else if ( strcmp( fextension( targetfileselect ) , "pptx" ) == 0 )
                 nrunwith( "  screen -d -m  libreoffice    " , targetfileselect );


             else if ( strcmp( fextension( targetfileselect ) , "ppt" ) == 0 )
                 nrunwith( "  screen -d -m  libreoffice    " , targetfileselect );


            else if ( strcmp( fextension( targetfileselect ) , "ctg" ) == 0 )
            {
                  printf(  "TI EDU.\n" );
                  nrunwith( " ti99sim-sdl -4 --scale2x -f  ", targetfileselect );
            }

            else if ( strcmp( fextension( targetfileselect ) , "atr" ) == 0 )
            {
                  //   atari800  -xlxe_rom  BIOS/atarixl.rom -fullscreen   -car   "Montezuma's Revenge _ Parker Brothers.atr" 
		  // Fullscreen AltL+F, working with resizing on rpi3b!
                  // Example: atari800  -xlxe_rom  ~/BIOS/atarixl.rom -fullscreen  -car   "Atari Invaders (1981)(Joe Helleson).atr" 
		  //
                  printf(  "Atari800 EDU.\n" );
                  printf(  "Newer atari800xe EDU.\n" );
                  printf(  "Keyboard: numpad <- 4, v 5, -> 6, up 8 and FIRE Right Ctrl \n");
                  printf(  "To start the atari800: Press F4 key.\n" );
                  printf(  "Learn BASIC! \n" );
                  nrunwith( "   atari800  -xlxe_rom ~/BIOS/atarixl.rom -fullscreen   -car    ", targetfileselect );
            }


            else if ( strcmp( fextension( targetfileselect ) , "gif" ) == 0 )
               nrunwith( "      feh  " , targetfileselect );


            else if ( strcmp( fextension( targetfileselect ) , "html" ) == 0 )
               nrunwith( "   links  " , targetfileselect );




             else if ( strcmp( fextension( targetfileselect ) , "jpg" ) == 0 )
               nrunwith( "      feh -FZ  " , targetfileselect );





             /// office stuffs
             else if ( strcmp( fextension( targetfileselect ) , "xls" ) == 0 )
               nrunwith( " screen -d -m    gnumeric " , targetfileselect );



             else if ( strcmp( fextension( targetfileselect ) , "xlsx" ) == 0 )
               nrunwith( "  screen -d -m   gnumeric " , targetfileselect );
             else if ( strcmp( fextension( targetfileselect ) , "doc" ) == 0 )
               nrunwith( " screen -d -m  libreoffice " , targetfileselect );
             else if ( strcmp( fextension( targetfileselect ) , "docx" ) == 0 )
               nrunwith( " screen -d -m  libreoffice " , targetfileselect );




             else if ( strcmp( fextension( targetfileselect ) , "epub" ) == 0 )
             { 
                nrunwith( " mupdf " , targetfileselect );
             } 

             /// office stuffs
             else if ( strcmp( fextension( targetfileselect ) , "pdf" ) == 0 )
             { 
                printf( "\nPath: %s\n" , getcwd( cwd, PATH_MAX ) );
                if      ( var_run_x  == 3 ) 
                 xnrunwith( "  export DISPLAY=:0 ;  screen -d -m  mupdf -r 250  " , targetfileselect );
                else if ( var_run_x  == 4 ) 
                 xnrunwith( "  export DISPLAY=:0 ;  mupdf -r 250  " , targetfileselect );
                else 
                 xnrunwith( "     mupdf  " , targetfileselect );
             } 


             else if ( strcmp( fextension( targetfileselect ) , "PDF" ) == 0 )
                xnrunwith( " mupdf " , targetfileselect );


            else if ( strcmp( fextension( targetfileselect ) , "eps" ) == 0 )
            {  
                strncpy( cwd , "  " , PATH_MAX );
                strncat( cwd , " cat " ,  PATH_MAX - strlen( cwd ) -1 );
                strncat( cwd , " \"" ,    PATH_MAX - strlen( cwd ) -1 );
                strncat( cwd , targetfileselect ,      PATH_MAX - strlen( cwd ) -1 );
                strncat( cwd , "\" |  epstopdf --filter > /tmp/test.pdf  " , PATH_MAX - strlen( cwd ) -1 );
                strncat( cwd , "  ; mupdf /tmp/test.pdf  " , PATH_MAX - strlen( cwd ) -1 );
                printf( "COMMAND: %s\n", cwd ); 
                nsystem( cwd ); 
            }


             else if ( strcmp( fextension( targetfileselect ) , "svg" ) == 0 )
                xnrunwith( " inkscape " , targetfileselect );

             else if ( strcmp( fextension( targetfileselect ) , "wad" ) == 0 )
               xnrunwith( "    prboom-plus -iwad " , targetfileselect );


             else if ( strcmp( fextension( targetfileselect ) , "avi" ) == 0 )
               nrunwith( " mplayer " , targetfileselect );


             else if ( strcmp( fextension( targetfileselect ) , "wav" ) == 0 )
             {
                  nrunwith( " aucat -i " , targetfileselect );
             }

             else if ( strcmp( fextension( targetfileselect ) , "mp3" ) == 0 )
             {
                  nrunwith( " mpg321 " , targetfileselect );
             }


             else if ( strcmp( fextension( targetfileselect ) , "zip" ) == 0 )
             {
                enable_waiting_for_enter();
                clear_scr();
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " unzip -l " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\" | less " , PATH_MAX - strlen( string ) -1 );
                nsystem( string );  

                ansigotoyx( rows-1, 0 );
                disable_waiting_for_enter();
		printf( "> Please press Enter <Keypress>.\n" );
                ch = getchar();
             }


             else 
	     {
	       printf( "\n" );
	       printf( "Launcher automatic...\n" );
	       printf( "\n" );
               nrunwith( " termautorun  " , targetfileselect );
	     }

       }




       //////////////////////////
       else if ( ch == 'c') 
       {
             if      ( color_mode[pansel] == 1 ) 
                color_mode[pansel] = 2; 
             else if ( color_mode[pansel] == 2 ) 
                color_mode[pansel] = 3; 
             else if ( color_mode[pansel] == 3 ) 
                color_mode[pansel] = 4; 
             else if ( color_mode[pansel] == 4 ) 
                color_mode[pansel] = 1; 
       }

       //////////////////////////
       else if ( ch == 'p') 
       {
            readfilesp( targetfileselect , 0 , rows-4 );
            getchar();
       }



       else if ( ( ch == 'o') && ( pansel == 1 )   )
       {
            chdir( pathpan[ 1 ] );
            chdir( targetfileselect );
            nexp_user_sel[ 2 ] = 1; 
            nexp_user_scrolly[ 2 ] = 0; 
            strncpy( pathpan[ 2 ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
       }

       else if ( ( ch == 'o') && ( pansel == 2 )   )
       {
            chdir( pathpan[ 2 ] );
            chdir( targetfileselect );
            nexp_user_sel[ 1 ] = 1; 
            nexp_user_scrolly[ 1 ] = 0; 
            strncpy( pathpan[ 1 ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
       }



       else if ( ch == 'k')      nexp_user_sel[pansel]--;
       else if ( ch == 'j')      nexp_user_sel[pansel]++;

       else if ( ch == 'g')      
       {
           ch = getchar();
           if ( ch == 'g' )
           { nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; }

           else if ( ch == '1' )
           {  if ( viewpan[ 1 ] == 1 )   viewpan[ 1 ] = 0; else viewpan[ 1 ] = 1; }
           else if ( ch == '2' )
           { if ( viewpan[ 2 ] == 1 )    viewpan[ 2 ] = 0; else viewpan[ 2 ] = 1; }

            else if ( ch == 't' ) 
            {
                if (   show_path_title == 0 ) 
                   show_path_title = 1;
                else
                   show_path_title = 0;
            }




            else if ( ch == 'b' ) 
            {
                if (   var_dsp_bottomstatus == 0 ) 
                   var_dsp_bottomstatus = 1;
                else
                   var_dsp_bottomstatus = 0;
            }
            ch = 0;
       }
       else if ( ch == 'G')      { nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; }
       else if ( ch == 'u')      nexp_user_scrolly[pansel]-=4;
       else if ( ch == 'd')      nexp_user_scrolly[pansel]+=4;
       else if ( ch == 'n')      nexp_user_scrolly[pansel]+=4;

  

       else if ( ch == 'T' ) 
       {
           if (   tc_det_dir_type == 0 ) 
              tc_det_dir_type = 1;
           else
              tc_det_dir_type = 0;
       }



       else if ( ch == 'v' )
       {  enable_waiting_for_enter();  nrunwith(  " vim  ",  targetfileselect    );   }


        else if ( ( ch == '!') || ( ch == 18 ) ) // ctrl+t
        {
            strninput( " Run Cmd on File (!) ", "" );
            strncpy( string, userstr , PATH_MAX );
            printf("\n" );
            printf("\n" );
            printf("got: \"%s\"\n", string );
            if ( strcmp( string , "" ) != 0 ) 
            {
               nrunwith( string , targetfileselect  ) ; 
            }
        }
        else if ( ch == '$' )   // S like silent
        {
            strninput( " Run SH Command ", "" );
            strncpy( string, userstr , PATH_MAX );
            printf("\n" );
            printf("\n" );
            printf("got: \"%s\"\n", string );
            if ( strcmp( string , "" ) != 0 ) 
            {
               printf("run: \"%s\"\n", string );
               enable_waiting_for_enter();
               nsystem( string );
               disable_waiting_for_enter();
               //getchar();
            }
            ch = 0; 
        }

        else if ( ch == '&' ) 
        {
            strninput( " Run SH Command ", "" );
            strncpy( string, userstr , PATH_MAX );
            printf("\n" );
            printf("\n" );
            printf("got: \"%s\"\n", string );
            if ( strcmp( string , "" ) != 0 ) 
            {
               printf("run: \"%s\"\n", string );
               enable_waiting_for_enter();
               nsystem( string );
               disable_waiting_for_enter();
               printf("<Key Press>\n" );
               getchar();
            }
            ch = 0; 
        }


       else if ( ch == ';' ) 
       {
            strninput( " Run CHDIR Command ", "" );
            strncpy( string, userstr , PATH_MAX );
            printf("\n" );
            printf("\n" );
            printf("got: \"%s\"\n", string );
            chdir( pathpan[ pansel ] );
            chdir( string );
            strncpy( pathpan[ pansel ] , getcwd( string, PATH_MAX ), PATH_MAX );
            nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
       }


       else if ( ch == 27 ) 
       {
          ch = getchar();
          if ( ch == 91 )  
          {
            ch = getchar();
            if ( ch == 49 )  
            {
              ch = getchar();
              if ( ch == 57 )  
              {
                 ch = getchar();
                 if ( ch == 126 )  
                 {
                     printf( "\n" );
                     printf( "F8\n" );
                     printf( "\n" );
                     //getchar();
                 }
              }
            }
          }
       }


      // older
      else if ( ch == 'W')      
      {
            chdir( pathpan[ pansel ] );
            chdir( getenv( "HOME" ) );
            chdir( "workspace" );
            nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
            strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
       }


       else if ( ch == 'q')   
       {
            ch = 0;
            gfxrect(      rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            gfxframe(     rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            mvcenter(     rows*30/100, "| CONFIRMATION |");
            foo = 1;
            snprintf( string , PATH_MAX , "Unix Time: %d", (int)time(NULL));
            printat(   rows*30/100 +foo++ , cols*30/100+1 , string );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , " 1 or y: Quit! " );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , " 0 or other key: Cancel " );
            foo = 1;
            ansigotoyx(  rows*70/100 , cols*70/100 );
            ch = getchar();
            ansigotoyx( rows-1, 0 );
            if   ( ch == '1' )      gameover  = 1;
            ch = 0;
       }






       else if ( ch == 'm')   // mem vim usage
       {
            ch = 0;
            gfxrect(      rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            gfxframe(     rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            mvcenter(     rows*30/100, "| MENU MEDIA |");
            foo = 1;
            snprintf( string , PATH_MAX , "Unix Time: %d", (int)time(NULL));
            printat(   rows*30/100 +foo++ , cols*30/100+1 , string );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , " (s): save dir paths" );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , " (n): save dir paths" );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , " (l): load dir paths" );
            foo = 1;
            ansigotoyx(  rows*70/100 , cols*70/100 );
            ch = getchar();
            ansigotoyx( rows-1, 0 );

            if   ( ch == 'l' ) proc_load_env( ".davepanel.ini" );
            else if   ( ch == 's' ) proc_save_env( ".davepanel.ini" );
            else if   ( ch == 'n' ) proc_save_env( ".davepanel.ini" );
            else if   ( ch == 'n' ) proc_save_env( ".davepanel.ini" );

            else if (  (  ( ch - 48 ) >= 1 ) &&  (  ( ch - 48 ) <= 9 ) )
	    {
		    strncpy( string , getcwd( cwd, PATH_MAX ), PATH_MAX );
		    //chdir( getenv( "HOME" ) );
		    //for( i = 1 ; i <= ch-48 ; i++)
		    //fgets( getlinestr , PATH_MAX, fptt ); 
		    //fclose( fptt );
		    printf( "[Save Path] Press Key... \n" );
		    //ch = getchar(); 
		    //if (  ( ch - 48 ) >= 1 )
		    //if (  ( ch - 48 ) <= 9 )
		    //{
		    foo = ch - 48;
		    chdir( pathpan[ pansel ] );
		    strncpy( dirslot[ foo ] , getcwd( string, PATH_MAX ), PATH_MAX );
		    printf( "\n");
		    for ( i = 1 ; i <= 10 ; i++ )
			    printf( "[%d] %s\n", i, dirslot[ i ]);
		    ch = getchar();
		    ansigotoyx( rows-1, 0 );
		    //}
		    //printf( "\n%s\n", getlinestr );
		    //for( i = 0; i < strlen( getlinestr ); i++ )
		    //{
		    //	    if( getlinestr[i] == '\n')  getlinestr[i] = '\0';
		    //}
		    //chdir( getlinestr ); 
		    //nexp_user_sel[ pansel ]=1; nexp_user_scrolly[ pansel ] = 0; 
		    //strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
                    chdir( string );
		    ch = 0;
	    }
            ch = 0;
       }







       else if ( ch == 'z')   // zoom media
       {
            ch = 0;
            gfxrect(      rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            gfxframe(     rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            mvcenter(     rows*30/100, "| MENU MEDIA |");
            foo = 1;
            snprintf( string , PATH_MAX , "Unix Time: %d", (int)time(NULL));
            printat(   rows*30/100 +foo++ , cols*30/100+1 , string );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "p: play mp3s " );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "g: gpicview " );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "m: mupdf " );
            foo = 1;
            ansigotoyx(  rows*70/100 , cols*70/100 );
            ch = getchar();
            ansigotoyx( rows-1, 0 );

            if  ( ch == 'p' )  
	    {
		nsystem( " mpg123 *.mp3"  );
                ch = 0 ; 
	    }
            else if  ( ch == 'm' )  
                nrunwith( " mupdf " , targetfileselect  );
            else if  ( ch == 'g' )  
                nrunwith( " gpicview " , targetfileselect  );
	    ch = 0; 
      }








        ///////////////
        else if ( ch == '\"')   
        {
            printf( "\n");
            for ( i = 1 ; i <= 10 ; i++ )
               printf( "[%d] %s\n", i, dirslot[ i ]);
            printf( "[Save Path] Press Key... \n" );
            ch = getchar(); 
            if (  ( ch - 48 ) >= 1 )
            if (  ( ch - 48 ) <= 9 )
            {
                foo = ch - 48;
                chdir( pathpan[ pansel ] );
                strncpy( dirslot[ foo ] , getcwd( string, PATH_MAX ), PATH_MAX );
            }
        }


        ///////////////
        else if ( ch == '\'')   
        {
            printf( "\n");
            for ( i = 1 ; i <= 10 ; i++ )
               printf( "[%d] %s\n", i, dirslot[ i ]);
            printf( "[Load Path] Press Key... \n" );
            ch = getchar(); 
            if (  ( ch - 48 ) >= 1 )
            if (  ( ch - 48 ) <= 9 )
            {
                foo = ch - 48;
                nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; //gg 
                chdir( dirslot[ foo  ] );
                strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
            }
        }





      ///////////////
      else if  ( ch == '"' ) 
      {
          printf( "\n");
          printf( "Press Key... \n" );
          ch = getchar(); 
          printf( "\nKEY Int:%d char:%c\n", ch , ch );
          printf( "\nKEY Int-48:%d char:%c\n", ch -48 , ch );
          strncpy( string , getcwd( cwd, PATH_MAX ), PATH_MAX );
          chdir( getenv( "HOME" ) );
          if ( filecheckexist( ".davepanel.ini" ) == 1 )
          if (  ( ch - 48 ) >= 1 )
          if (  ( ch - 48 ) <= 9 )
          {
               fptt = fopen( ".davepanel.ini", "rb+" );
               for( i = 1 ; i <= ch-48 ; i++)
                 fgets( getlinestr , PATH_MAX, fptt ); 
               fclose( fptt );
               printf( "\n%s\n", getlinestr );
               for( i = 0; i < strlen( getlinestr ); i++ )
               {
                    if( getlinestr[i] == '\n')  getlinestr[i] = '\0';
               }
               chdir( getlinestr ); 
               nexp_user_sel[ pansel ]=1; nexp_user_scrolly[ pansel ] = 0; 
               strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
          }
          chdir( string );
      }


      // older
      else if ( ch == 'w')      
      {
            chdir( pathpan[ pansel ] );
            chdir( getenv( "HOME" ) );
            chdir( "workspace" );
            nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
            strncpy( pathpan[ pansel ] , getcwd( cwd, PATH_MAX ), PATH_MAX );
       }





       else if ( ch == ':' ) 
       {
            clear_scr( );
            ansigotoyx( rows/2-1 , 0 );
            printhline( );
            ansigotoyx( rows/2 , 0 );
            printf( "Interpreter and commands\n" );
            ansigotoyx( rows/2+1 , 0 );
            printf("\n" );
            strninput( "", "" );
            strncpy( string, userstr , PATH_MAX );
            printf("got: \"%s\"\n", string );


            printf("User cmd: \"%s\"\n", string );
            enable_waiting_for_enter();

            if ( strcmp( string, "size" ) == 0 )  
            {
              printf("Screen\n" );
              printf("Env HOME:  %s\n", getenv( "HOME" ));
              printf("Env PATH:  %s\n", getcwd( string, PATH_MAX ) );
              printf("Env TERM ROW:  %d\n", w.ws_row );
              printf("Env TERM COL:  %d\n", w.ws_col );
              getchar();
            }    




            else if ( strcmp( string, "unixtime" ) == 0 )  
            {
                 ansigotoyx( rows-1 , 0 );
                 printhline( );
                 ansigotoyx( rows , 0 );
                 strninput( "", "" );
                 printf( "\n" );
                 time_t t = default_time;
                 struct tm lt;
                 t = (time_t) atoi(  userstr );
                 foo = 1;  /// new check 
                 ansigotoyx( foo++ , cols/ 2 );
                 printf( "TIME: %d\n" , (int)time(NULL));
                 (void) localtime_r(&t, &lt);
                 if (strftime( cmdifoo, sizeof(cmdifoo), "%c", &lt) == 0) 
                 {}
                 ansigotoyx( foo++ , cols/ 2 );
                 (void) printf("%u -> '%s'\n", (unsigned) t, cmdifoo);
                 ansigotoyx( rows-1, 0 ); printf( "<Press Key>" );
                 getchar();
                 strncpy( string, "" , PATH_MAX );
            }    




            else if ( strcmp( string, "time" ) == 0 )  
            {
              gfxrect(   (int)  rows*30/100 ,  (int) cols*30/100, rows*70/100, cols*70/100 );
              gfxframe(  (int)  rows*30/100 ,  (int)  cols*30/100, rows*70/100, cols*70/100 );
              mvcenter(  (int)  rows*30/100,   "| MBOX |");
              foo = rows/2;
              ansigotoyx( foo++ , cols/ 2 );
              printf( "TIME: %d\n" , (int)time(NULL));
              ansigotoyx( rows-1, 0 ); printf( "<Press Key>" );
              getchar();
            }    



            else if ( strcmp( string, "box" ) == 0 )  
            {
              printf("Box\n" );
              printf("Env HOME:  %s\n", getenv( "HOME" ));
              printf("Env PATH:  %s\n", getcwd( string, PATH_MAX ) );
              printf("Env TERM ROW:  %d\n", w.ws_row );
              printf("Env TERM COL:  %d\n", w.ws_col );
              gfxrect(   (int)  rows*30/100 ,  (int) cols*30/100, rows*70/100, cols*70/100 );
              gfxframe(  (int)  rows*30/100 ,  (int)  cols*30/100, rows*70/100, cols*70/100 );
              mvcenter(  (int)  rows*30/100,   "| MBOX |");
              getchar();
            }    

            else if ( ( strcmp( string, "loadkeys" ) == 0 )  || ( strcmp( string, "keys" ) == 0 ) )
            {
              printf("Box\n" );
              printf("Env HOME:  %s\n", getenv( "HOME" ));
              printf("Env PATH:  %s\n", getcwd( string, PATH_MAX ) );
              printf("Env TERM ROW:  %d\n", w.ws_row );
              printf("Env TERM COL:  %d\n", w.ws_col );
              gfxrect(   (int)  rows*30/100 ,  (int) cols*30/100, rows*70/100, cols*70/100 );
              gfxframe(  (int)  rows*30/100 ,  (int)  cols*30/100, rows*70/100, cols*70/100 );
              mvcenter(  (int)  rows*30/100,   "| MD5 |");
              nrunwith( " loadkeys "  , targetfileselect  ) ; 
              getchar();
            }    

            else if ( ( strcmp( string, "md5" ) == 0 )  || ( strcmp( string, "md5sum" ) == 0 ) )
            {
              printf("Box\n" );
              printf("Env HOME:  %s\n", getenv( "HOME" ));
              printf("Env PATH:  %s\n", getcwd( string, PATH_MAX ) );
              printf("Env TERM ROW:  %d\n", w.ws_row );
              printf("Env TERM COL:  %d\n", w.ws_col );
              gfxrect(   (int)  rows*30/100 ,  (int) cols*30/100, rows*70/100, cols*70/100 );
              gfxframe(  (int)  rows*30/100 ,  (int)  cols*30/100, rows*70/100, cols*70/100 );
              mvcenter(  (int)  rows*30/100,   "| MD5 |");
                 nrunwith( " md5sum  "  , targetfileselect  ) ; 
                 //nrunwith( " md5  "  , targetfileselect  ) ; 
              getchar();
            }    


            else if ( strcmp( string, "du" ) == 0 )  
            {
              printf("Box\n" );
              printf("Env HOME:  %s\n", getenv( "HOME" ));
              printf("Env PATH:  %s\n", getcwd( string, PATH_MAX ) );
              printf("Env TERM ROW:  %d\n", w.ws_row );
              printf("Env TERM COL:  %d\n", w.ws_col );
              gfxrect(   (int)  rows*30/100 ,  (int) cols*30/100, rows*70/100, cols*70/100 );
              gfxframe(  (int)  rows*30/100 ,  (int)  cols*30/100, rows*70/100, cols*70/100 );
              mvcenter(  (int)  rows*30/100,   "| DU SIZE |");
              nrunwith( " du -hs "  , targetfileselect  ) ; 
              getchar();
            }    

            else if ( strcmp( string, "reboot" ) == 0 )  
                 nsystem( " reboot ; sudo reboot " ); 
            else if ( strcmp( string, "REBOOT" ) == 0 ) 
                 nsystem( " reboot ; sudo reboot " ); 
            else if ( strcmp( string, "shutdown" ) == 0 )  nsystem( " shutdown -p now " ); 
            else if ( strcmp( string, "SHUTDOWN" ) == 0 )  nsystem( " shutdown -p now " ); 




            else if ( strcmp( string, "path" ) == 0 )  
            {
               printf( "PATH: (%s)\n" , getcwd( cmdifoo, PATH_MAX ) );
               disable_waiting_for_enter();
               getchar();
            }    



            else if ( strcmp( string, "f1" ) == 0 ) 
            {
                strncpy(  fileclip_selection[ 1 ], getcwd( cmdifoo , PATH_MAX ) , PATH_MAX );
                strncat(  fileclip_selection[ 1 ] , "/" , PATH_MAX - strlen( fileclip_selection[ 1 ] ) -1 );
                strncat(  fileclip_selection[ 1 ] , targetfileselect , PATH_MAX - strlen( fileclip_selection[ 1 ] ) -1 );
            }
            else if ( strcmp( string, "f2" ) == 0 ) 
            {
                strncpy(  fileclip_selection[ 2 ], getcwd( cmdifoo , PATH_MAX ) , PATH_MAX );
                strncat(  fileclip_selection[ 2 ] , "/" , PATH_MAX - strlen( fileclip_selection[ 2 ] ) -1 );
                strncat(  fileclip_selection[ 2 ] , targetfileselect , PATH_MAX - strlen( fileclip_selection[ 2 ] ) -1 );
            }
            else if ( strcmp( string, "f3" ) == 0 ) 
            {
                strncpy(  fileclip_selection[ 3 ], getcwd( cmdifoo , PATH_MAX ) , PATH_MAX );
                strncat(  fileclip_selection[ 3 ] , "/" , PATH_MAX - strlen( fileclip_selection[ 3 ] ) -1 );
                strncat(  fileclip_selection[ 3 ] , targetfileselect , PATH_MAX - strlen( fileclip_selection[ 3 ] ) -1 );
            }
            else if ( strcmp( string, "f4" ) == 0 ) 
            {
                strncpy(  fileclip_selection[ 4 ], getcwd( cmdifoo , PATH_MAX ) , PATH_MAX );
                strncat(  fileclip_selection[ 4 ] , "/" , PATH_MAX - strlen( fileclip_selection[ 4 ] ) -1 );
                strncat(  fileclip_selection[ 4 ] , targetfileselect , PATH_MAX - strlen( fileclip_selection[ 4 ] ) -1 );
            }



            else if ( ( strcmp( string, "file3" ) == 0 )  || ( strcmp( string, "f3" ) == 0 )    )
                strncpy(  fileclip_selection[ 3 ], targetfileselect , PATH_MAX );

            else if ( ( strcmp( string, "file4" ) == 0 )  || ( strcmp( string, "f4" ) == 0 )    )
                strncpy(  fileclip_selection[ 4 ], targetfileselect , PATH_MAX );



            else if ( strcmp( string, "keycheck" ) == 0 )  
            {
                 printf( "\n");
                 printf( "Press Key... \n" );
                 ch = getchar(); 
                 printf( "\nKEY Int:%d char:%c\n", ch , ch );
                 printf( "\nKEY Int-48:%d char:%c\n", ch -48 , ch );
                 ch = getchar(); 
            }


            else if ( strcmp( string, "!unlink" ) == 0 )
            {
                printf(  "Force unlink (sys).\n" );
                unlink(   targetfileselect    );   
            }


            else if ( strcmp( string, "set" ) == 0 ) 
            {
               enable_waiting_for_enter();
               printf( "-=-=-=-\n" ); 
               system( "  echo DSP $DISPLAY  " ); 
               system( "  echo HOME $HOME  " ); 
               system( "  echo USER $USER  " ); 
               printf( " Int. File: %s\n",  targetfileselect    );   
               printf( " Int. HOME: %s\n",  getenv( "HOME" )  );   
               printf( " Int. CWD:  %s\n",  getcwd( cwd, PATH_MAX )  );   
               printf( "-=-=-=-\n" ); 
               disable_waiting_for_enter();
               getchar();
            }

            else if ( strcmp( string, "less" ) == 0 )
                nrunwith(  " less ",  targetfileselect    );   






            /////////////////////////////////////////////////
            else if ( strcmp( string, "mkmd5" ) == 0 )
            {
                enable_waiting_for_enter();
                clear_scr();
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " md5sum  " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , "   \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\"  >>  /tmp/list.md5 " , PATH_MAX - strlen( string ) -1 );
		nsystem( string ); 
                disable_waiting_for_enter();
            }
            /////////////////////////////////////////////////





            /////////////////////////////////////////////////
            else if ( strcmp( string, "svndel" ) == 0 )
            {
                enable_waiting_for_enter();
                clear_scr();
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " svn delete  " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , "   \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\"  --force  " , PATH_MAX - strlen( string ) -1 );
		nsystem( string ); 
                disable_waiting_for_enter();
            }
            /////////////////////////////////////////////////


            else if ( strcmp( string, "disklabel" ) == 0 )  
            {
                enable_waiting_for_enter();
                clear_scr();
                home(); 
                ansigotoyx( 1 , 0 );
                printhline( );
                nsystem( " cd ; fdisk sd0 ; disklabel sd0  "  );  
                disable_waiting_for_enter();
                printf( "Press Key... \n" ); 
                ch = getchar(); 
		printf( "\nKEY %d %c\n", ch , ch );
                clear_scr();
                disable_waiting_for_enter();
	    }

            else if ( strcmp( string, "mdf" ) == 0 )  
            {
                enable_waiting_for_enter();
                clear_scr();
                home(); 
                ansigotoyx( 1 , 0 );
                printhline( );
                nsystem( " mount  "  );  
                nsystem( " df -h  "  );  
                disable_waiting_for_enter();
                printf( "Press Key... \n" ); 
                ch = getchar(); 
		printf( "\nKEY %d %c\n", ch , ch );
                clear_scr();
                disable_waiting_for_enter();
	    }

            else if ( strcmp( string, "df" ) == 0 )  
            {
                enable_waiting_for_enter();
                clear_scr();
                home(); 
                ansigotoyx( 1 , 0 );
                printhline( );
                nsystem( " df -h  "  );  
                disable_waiting_for_enter();
                printf( "Press Key... \n" ); 
                ch = getchar(); 
		printf( "\nKEY %d %c\n", ch , ch );
                clear_scr();
                disable_waiting_for_enter();
	    }


            else if ( strcmp( string, "mount" ) == 0 )  
            {
                enable_waiting_for_enter();
                clear_scr();
                home(); 
                ansigotoyx( 1 , 0 );
                printhline( );
                nsystem( " mount "  );  
                disable_waiting_for_enter();
                printf( "Press Key... \n" ); 
                ch = getchar(); 
		printf( "\nKEY %d %c\n", ch , ch );
                clear_scr();
                disable_waiting_for_enter();
	    }

            else if ( strcmp( string, "edit" ) == 0 )  
	    {
                enable_waiting_for_enter();
                clear_scr();
                nrunwith( " screen -d -m flnotepad  ", targetfileselect );
                disable_waiting_for_enter();
	    }



            else if ( strcmp( string, "xterm" ) == 0 )  
	    {
                enable_waiting_for_enter();
                clear_scr();
                nsystem( " xterm " );  
                disable_waiting_for_enter();
	    }

            else if ( strcmp( string, "clipboard" ) == 0 )  
	    {
                enable_waiting_for_enter();
                clear_scr();
                nsystem( " vim ~/.clipboard " );
                disable_waiting_for_enter();
	    }

            else if ( strcmp( string, "env" ) == 0 )  
	    {
                enable_waiting_for_enter();
                printf( " HOME %s \n" , getenv( "HOME" )); 
                printf( " USER %s \n" , getenv( "USER" )); 
                disable_waiting_for_enter();
	        getchar();
	    }



            else if ( strcmp( string, "vimclip" ) == 0 )  
	    {
                enable_waiting_for_enter();
                printf( " HOME %s \n" , getenv( "HOME" )); 
                printf( " USER %s \n" , getenv( "USER" )); 
                strncpy( foostr , getenv( "HOME" ), PATH_MAX );
                strncat( foostr , "/.clipboard" , PATH_MAX - strlen( foostr ) -1 );
		strncpy( fileclip_selection[ 4 ],  foostr, PATH_MAX ); 
                printf( "File %s %d\n", targetfileselect, filecheckexist( targetfileselect )  ); 
                printf( "Press Key... \n" );
                disable_waiting_for_enter();
	        getchar();
	    }




            else if ( ( strcmp( string, "vim" ) == 0 )  
            || ( strcmp( string, "multi" ) == 0 )  
            || ( strcmp( string, "merge" ) == 0 )  
            || ( strcmp( string, "vim" ) == 0 )  )
            {
                enable_waiting_for_enter();
                clear_scr();
	        strncpy( foostr, string, PATH_MAX ); 
                strncpy( string , " vim -p  " , PATH_MAX );

                if ( strcmp( foostr , "vim" ) == 0 )  
		{
		  // set vim by default 
		  // rem
		}
  
                if ( filecheckexist( targetfileselect ) == 1 )
	        {
                  strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                  strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                  strncat( string , "\"  " , PATH_MAX - strlen( string ) -1 );
	        }

                if ( strcmp( foostr , "merge" ) == 0 )  
                   strncpy( string , " cat  " , PATH_MAX ); 

                if ( strcmp( fileclip_selection[ 1 ],  ""  ) != 0 ) 
                {
                  strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                  strncat( string ,  fileclip_selection[ 1 ] , PATH_MAX - strlen( string ) -1 );
                  strncat( string , "\"  " , PATH_MAX - strlen( string ) -1 );
                }
                if ( strcmp( fileclip_selection[ 2 ],  ""  ) != 0 ) 
                {
                  strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                  strncat( string ,  fileclip_selection[ 2 ] , PATH_MAX - strlen( string ) -1 );
                  strncat( string , "\"  " , PATH_MAX - strlen( string ) -1 );
                }
                if ( strcmp( fileclip_selection[ 3 ],  ""  ) != 0 ) 
                {
                  strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                  strncat( string ,  fileclip_selection[ 3 ] , PATH_MAX - strlen( string ) -1 );
                  strncat( string , "\"  " , PATH_MAX - strlen( string ) -1 );
                }
                if ( strcmp( fileclip_selection[ 4 ],  ""  ) != 0 ) 
                {
                  strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                  strncat( string ,  fileclip_selection[ 4 ] , PATH_MAX - strlen( string ) -1 );
                  strncat( string , "\"  " , PATH_MAX - strlen( string ) -1 );
                }


                if ( strcmp( foostr , "merge" ) == 0 )  
                  strncat( string , " | vim - " , PATH_MAX - strlen( string ) -1 );
                nsystem( string );  
                disable_waiting_for_enter();
            }




            else if ( strcmp( string, "clear" ) == 0 )  
            {
                strncpy( fileclip_selection[ 1 ] , "",  PATH_MAX);
                strncpy( fileclip_selection[ 2 ] , "",  PATH_MAX);
                strncpy( fileclip_selection[ 3 ] , "",  PATH_MAX);
                strncpy( fileclip_selection[ 4 ] , "",  PATH_MAX);
		nsystem( " clear "); 
                disable_waiting_for_enter();
	    }

            else if ( strcmp( string, "save" ) == 0 )  
            {
                      strncpy( string , getcwd( cwd, PATH_MAX ), PATH_MAX );
                      chdir( getenv( "HOME" ) );

                      fptt = fopen( ".davepanel.ed", "wb" );
                      fclose( fptt );

                      fptt = fopen( ".davepanel.ed", "ab" );
		      fputs( fileclip_selection[ 1 ] , fptt );
		      fputs( "\n" , fptt );
		      fputs( fileclip_selection[ 2 ] , fptt );
		      fputs( "\n" , fptt );
		      fputs( fileclip_selection[ 3 ] , fptt );
		      fputs( "\n" , fptt );
		      fputs( fileclip_selection[ 4 ] , fptt );
		      fputs( "\n" , fptt );
                      for( ch = 1 ; ch <= 10 ; ch++)
                      {
		          fputs( dirslot[ ch ] , fptt );
		          fputs( "\n" , fptt );
                      }
                      fclose( fptt );
                      chdir( string );
                      ch = 0;
             }

            else if ( strcmp( string, "load" ) == 0 )  
            {
                      strncpy( string , getcwd( cwd, PATH_MAX ), PATH_MAX );
                      chdir( getenv( "HOME" ) );
		      if ( filecheckexist( ".davepanel.ed" ) == 1 )
		      {
                       fptt = fopen( ".davepanel.ed", "rb" );
                       for( ch = 1 ; ch <= 4 ; ch++)
		       {
                         fgets( getlinestr , PATH_MAX, fptt ); 
                         printf( "\n%s\n", getlinestr );
                         for( i = 0; i < strlen( getlinestr ); i++ )
                            if( getlinestr[i] == '\n')  getlinestr[i] = '\0';
		         strncpy( fileclip_selection[ ch ] , getlinestr , PATH_MAX );
		       }
                       ///////////
                       for( ch = 1 ; ch <= 10 ; ch++)
		       {
                         fgets( getlinestr , PATH_MAX, fptt ); 
                         printf( "\n%s\n", getlinestr );
                         for( i = 0; i < strlen( getlinestr ); i++ )
                            if( getlinestr[i] == '\n')  getlinestr[i] = '\0';
		         strncpy( dirslot[ ch ] , getlinestr , PATH_MAX );
		       }
                       fclose( fptt );
		      }
                      chdir( string );
		      ch = 0; 
             }

            else if ( strcmp( string, "md5 *" ) == 0 )  
            {
                enable_waiting_for_enter();
                printf( "\nProcess '%s' cmd... \n" , string ); 
                nsystem( " md5sum * | less " );
                disable_waiting_for_enter();
            }

            else if ( strcmp( string, "sterm" ) == 0 )  
            {
                nsystem( " screen -d -m xterm  " );
                disable_waiting_for_enter();
            }

            else if ( strcmp( string, "rox" ) == 0 )  
            {
                nsystem( " rox " );
                disable_waiting_for_enter();
            }

            else if ( strcmp( string, "hostname" ) == 0 )  
            {
		 nsystem( " less /etc/hostname " );
                 printf( "Press Key... \n" ); 
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
                 disable_waiting_for_enter();
            }
            else if ( strcmp( string, "rc" ) == 0 )  
            {
		 nsystem( " less /etc/rc.conf " );
                 printf( "Press Key... \n" ); 
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
                 disable_waiting_for_enter();
            }

            else if ( strcmp( string, "color" ) == 0 )  
            {
                 printf( "Press Key... \n" );  void_print_colors();
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
                 disable_waiting_for_enter();
            }

            else if ( strcmp( string, "hello" ) == 0 )  
            {
                 printf( "Press Key... \n" );
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
                 disable_waiting_for_enter();
            }

            else if ( strcmp( string, "key" ) == 0 )  
            {
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
                 ch = getchar(); printf( "\nKEY %d %c\n", ch , ch );
            }
       }



       /// tab  /// THETAB TAB 
       else if ( ch == 9 ) 
       {  if ( pansel == 1 )   pansel = 2 ; else pansel = 1; }  


       else if ( ch == 'x' ) 
       {
           if ( strcmp(  file_filter[pansel] , "" ) != 0 ) 
           {
              strncpy( file_filter[pansel]  , "" , PATH_MAX );
              nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
           }
       }

       else if ( ch == 8 )  // backspace
       {
           if ( strcmp(  file_filter[pansel] , "" ) != 0 ) 
           {
              strncpy( file_filter[pansel]  , "" , PATH_MAX );
              nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0; 
           }
       }

       else if ( ( ch == 'f' ) || ( ch == '/' ) )
       {
            ansigotoyx( rows-1 , 0 );
            printhline( );
            ansigotoyx( rows , 0 );
            strninput( "", "" );
            strncpy( string, userstr , PATH_MAX );
            strncpy( file_filter[pansel]  , userstr , PATH_MAX );
            nexp_user_sel[pansel]=1; nexp_user_scrolly[pansel] = 0;
            printf("got: \"%s\"\n", string );
       }

       /// and key 'i'
       else if ( ch == 'i' )
       {  if ( pansel == 1 )   pansel = 2 ; else pansel = 1; }



       else if ( ch == '?' )
       {
           home(); 
           ansigotoyx( 1 , 0 );
           printhline( );

           home(); 
           ansigotoyx( 1 , 0 );
           mvcenter( 1 , fileexplorer_message );

           printf("%s", KCYN);
           gfxrect(   rows*10/100 ,         cols*10/100, rows*90/100, cols*90/100 );
           gfxframe(  rows*10/100 ,         cols*10/100, rows*90/100, cols*90/100 );
           mvcenter(  rows*10/100+1, "| HELP |");
           foo = 1;
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , "  v.0.14.3 ");
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , " ");
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , " ");
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , "  q: Quit");
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , "  hjkl: move file and dir ");
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , "  5:  copy file or dir " );
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , "  6:  move file or dir " );
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , "  7:  mkdir  " );
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , "  8:  touch  " );
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , "  !: run with (system command)");
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , "  $: system command");
           printat(   rows*10/100+1+foo++ , cols*10/100+1 , "  :: internal command");
           printf("%s", KWHT);

            //////////////////
            ansigotoyx( rows-1, 0 );
            for( foo = 0 ;  foo <= cols-1 ; foo++) printf( " " );
            ansigotoyx( rows-1, 0 ); printf( "<Press Key>" );
            disable_waiting_for_enter();
            getchar();
            ch = 0;
            //////////////////
       }





       // run it
       // 18 is 
       // 5 is 
       //   if ( ch == 18 )   // CTRL+R
       //    else if ( ch == 20 )   // CTRL+T to view cur file

       else if ( ch == 'A') 
       {
            // small size menu
            gfxrect(      rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            gfxframe(     rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            mvcenter(     rows*30/100, "| MENU |");
            foo = 1;
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "0: default (0)");
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "1: display :0 (1)");
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "2: display :0 and screen (2)");
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "3: :0, scr, mupdf -r 250 (3)");
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "4: :0, mupdf -r 250 (4)");
            ansigotoyx(  rows*70/100 , cols*70/100 );
            ansigotoyx( 0, 0 );
            printf( "======\n" );
            printf( "| %d  |\n", var_run_x );
            printf( "======\n" );
            ansigotoyx( rows-1, 0 );
            ch = getchar();
            ansigotoyx( rows-1, 0 );
            if           ( ch == '0' ) var_run_x = 0;
            else if      ( ch == '1' ) var_run_x = 1;
            else if      ( ch == '2' ) var_run_x = 2;
            else if      ( ch == '3' ) var_run_x = 3;
            else if      ( ch == '4' ) var_run_x = 4;
       }


       else if ( ch == 'Z') 
       {
           if ( var_panel_rcn_rcname == 0 ) 
              var_panel_rcn_rcname = 1; 
           else 
              var_panel_rcn_rcname = 0; 
       }


       else if ( ch == 5 )  // ctrl + e : explorer lf 
       {
               enable_waiting_for_enter();  
               nrunwith( " lf  ", targetfileselect );
               disable_waiting_for_enter();  
       }












       else if ( ch == 'r') 
       {
            ch = 0;
            gfxrect(      rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            gfxframe(     rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            mvcenter(     rows*30/100, "| MENU FILE |");
            foo = 1;
            snprintf( string , PATH_MAX , "Unix Time: %d", (int)time(NULL));
            printat(   rows*30/100 +foo++ , cols*30/100+1 , string );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "l or r: less" );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "u: unzip info" );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "p: xpaint" );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "x: mupdf :0" );
            foo = 1;
            ansigotoyx(  rows*70/100 , cols*70/100 );
            ch = getchar();
            ansigotoyx( rows-1, 0 );

            if  ( ch == 'l' )  
	    {
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " less  " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\" " , PATH_MAX - strlen( string ) -1 );
		nsystem( string );
                ch = 0 ; 
	    }

            else if  ( ch == 'u' )  
	    {
                ch = 0 ; 
                gfxrect(   (int)  rows*30/100 ,  (int) cols*30/100, rows*70/100, cols*70/100 );
                gfxframe(  (int)  rows*30/100 ,  (int)  cols*30/100, rows*70/100, cols*70/100 );
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " unzip -l  " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\" " , PATH_MAX - strlen( string ) -1 );
		nsystem( string );
		printf( "Press Key.\n" ); 
                getchar();
                ch = 0 ; 
	    }


            if  ( ch == 'x' )  
	    {
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " export DISPLAY=:0 ; mupdf  " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\" " , PATH_MAX - strlen( string ) -1 );
		nsystem( string );
                ch = 0 ; 
	    }

            else if  ( ch == 'r' )  
	    {
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " less -f  " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\" " , PATH_MAX - strlen( string ) -1 );
		nsystem( string );
                ch = 0 ; 
	    }
	    ch = 0; 
      }











      else if ( ch == 't') 
      {
            gfxrect(      rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            gfxframe(     rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            mvcenter(     rows*30/100, "| MENU |");
            foo = 1;
            snprintf( string , PATH_MAX , "Unix Time: %d, Color: %d-%d", (int)time(NULL), color_mode[1], color_mode[2] );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , string );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "d: svn delete sel      " );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "s: svn up " );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "k: loadkeys file (pandora)" );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "c: append to ~/.clipboard " );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "e: edit ~/.clipboard (vim)" );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "y: raw copy to ~/.clipboard" );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "x,X: mupdf (file)");
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "m: mount (system)");
            foo = 1;
            ansigotoyx(  rows*70/100 , cols*70/100 );
            ch = getchar();
            ansigotoyx( rows-1, 0 );


            if  ( ch == 'k' )  
	    {
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " loadkeys  " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\" " , PATH_MAX - strlen( string ) -1 );
		nsystem( string );
                ch = 0 ; 
	    }
            else if      ( ch == 'X' ) nrunwith( " export DISPLAY=:0 ; mupdf  "    , targetfileselect );
            else if      ( ch == 'x' ) nrunwith( "  mupdf  "    , targetfileselect );

            else if  ( ch == 's' )  
	    {
	        nsystem( " svn up " );
                ch = 0 ; 
	    }

            else if  ( ch == 'm' )  
	    {
                enable_waiting_for_enter();
                clear_scr();
                home(); 
                ansigotoyx( 1 , 0 );
                printhline( );
                nsystem( " mount "  );  
                disable_waiting_for_enter();
                printf( "Press Key... \n" ); 
                ch = getchar(); 
		printf( "\nKEY %d %c\n", ch , ch );
                clear_scr();
                disable_waiting_for_enter();
                ch = 0 ; 
	    }

            else if  ( ch == 'x' )  
	    {
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " export DISPLAY=:0 ; mupdf  " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\" " , PATH_MAX - strlen( string ) -1 );
		nsystem( string );
                ch = 0 ; 
	     }

            else if  ( ch == 'y' )  
	    {
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " cp   " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\" " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " ~/.clipboard " , PATH_MAX - strlen( string ) -1 );
                printf( "Proposed command: %s\n" , string );  
		printf( "Please confirm 1/y or N ?\n" );
		ch = '1';
                if ( ( ch == '1' ) || ( ch == 'y' ) )
		{
                   nsystem( string );  
		}
                ch = 0 ; 
	    }

            else if  ( ch == 'e' )  
	    {
                nsystem( " vim ~/.clipboard   "  );  
                ch = 0 ; 
	    }

            else if  ( ch == 'c' )  
	    {
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " cat   " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\" " , PATH_MAX - strlen( string ) -1 );
                strncat( string , "   >>   ~/.clipboard " , PATH_MAX - strlen( string ) -1 );
                printf( "Proposed command: %s\n" , string );  
		printf( "Please confirm 1/y or N ?\n" );
		ch = '1';
                if ( ( ch == '1' ) || ( ch == 'y' ) )
		{
                   nsystem( string );  
		}
                ch = 0 ; 
	    }


            else if  ( ch == 'd' )  
	    {
                clear_scr();
                strncpy( string , "  " , PATH_MAX );
                strncat( string , " svn delete  " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " " , PATH_MAX - strlen( string ) -1 );
                strncat( string , " \"" , PATH_MAX - strlen( string ) -1 );
                strncat( string ,  targetfileselect , PATH_MAX - strlen( string ) -1 );
                strncat( string , "\" --force " , PATH_MAX - strlen( string ) -1 );
                disable_waiting_for_enter();
                ansigotoyx( rows-1, 0 );
                printf( "Proposed command: %s\n" , string );  
		printf( "Please confirm 1/y or N ?\n" );
                ch = getchar();
                if ( ( ch == '1' ) || ( ch == 'y' ) )
		{
                   nsystem( string );  
		}
                ch = 0 ; 
	    }
            ch = 0;
       }






       else if ( ch == 'T') 
       {
            // small size menu
            gfxrect(      rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            gfxframe(     rows*30/100 , cols*30/100, rows*70/100, cols*70/100 );
            mvcenter(     rows*30/100, "| MENU |");
            foo = 1;
            snprintf( string , PATH_MAX , "Unix Time: %d", (int)time(NULL));
            printat(   rows*30/100 +foo++ , cols*30/100+1 , string );
            printat(   rows*30/100 +foo++ , cols*30/100+1 , "r: rox " );
            foo = 1;
            ansigotoyx(  rows*70/100 , cols*70/100 );
            ch = getchar();
            ansigotoyx( rows-1, 0 );
            if  ( ch == 'r' )  nsystem( " rox " );
            ch = 0;
       }

    }

    clear_scr();
    enable_waiting_for_enter(); 
    enable_waiting_for_enter();
    printf( "\n" );
    printf( "Bye!\n" );
    return 0;
}


/// EOF 




